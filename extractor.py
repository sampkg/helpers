import os
from config import EXTRACTOR_IMGS, EXTRACTOR_PATH
from progress import print_progress


def process(img_path):
    file_name = img_path.split("/")[-1]

    with open(img_path, 'rb') as img_file:
        signature = img_file.read(4).decode('ascii')

        if signature != "VER2":
            print("Некорректная сигнатура архива")
        else:
            num_files = int.from_bytes(img_file.read(4), byteorder='little')

            os.makedirs(EXTRACTOR_PATH, exist_ok=True)

            files = []

            for _ in range(num_files):
                # Читаем смещение файла (в блоках)
                offset = int.from_bytes(img_file.read(4), byteorder='little')

                # Читаем размер файла (в блоках)
                size = int.from_bytes(img_file.read(4), byteorder='little')

                # Читаем имя файла
                filename = img_file.read(24).decode('utf-8', errors='ignore').rstrip(
                    '\0')  # Удаляем нулевые байты в конце
                files.append((filename, offset, size))

            k = 1
            total = len(files)
            for f in files:
                filename, offset, size = f
                img_file.seek(offset * 2048)
                file_data = img_file.read(size * 2048)

                with open(f"{EXTRACTOR_PATH}/{filename}", 'wb') as extracted_file:
                    extracted_file.write(file_data)

                print_progress(
                    k,
                    total,
                    f"File: {filename}",
                    prefix=f"Extract {file_name} | "
                )
                k += 1


if __name__ == '__main__':
    for img in EXTRACTOR_IMGS:
        process(img)
