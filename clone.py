import os
import shutil
from config import EXTRACTOR_PATH, COPY_RESULT_TEMP, COPY_IDES
from progress import print_progress


def copy(file):
    source = EXTRACTOR_PATH + "/" + file
    destination = COPY_RESULT_TEMP + "/" + file

    if not os.path.isfile(source):
        print(f"[ERROR] - no file in {source}")
        return False
    if os.path.isfile(destination):
        return True
    shutil.copy(source, destination)
    return True


def process(ide):
    ide_name = ide.split("/")[-1]

    with open(ide) as f:
        data = f.read().strip().split("\n")
        founded = []
        current = ""

        for line in data:
            props = line.strip().split(", ")
            if len(props) == 1:
                current = props[0]
                continue

            if len(props) > 2:
                founded.append(
                    (current, props[1], props[2])
                )
            else:
                input(f"Unkown line in file {ide} - {line}")

        total = len(founded)
        k = 1
        for found in founded:
            kind, model, texture = found
            if not (kind == "objs" or kind == "tobj"):
                print("Sorry i can move it >", kind, model, texture)

            model_name = f"{model}.dff"
            texture_name = f"{texture}.txd"

            copy(model_name)
            copy(texture_name)

            print_progress(
                k,
                total,
                f"File: ({model_name}/{texture_name})",
                prefix=f"Extract {ide_name} | "
            )
            k += 1


if __name__ == "__main__":
    if input("Is dir => ").lower() == "y":
        for file in os.listdir(COPY_IDES):
            if not file.endswith(".ide"):
                continue
            process(COPY_IDES+"/"+file)

    else:
        for ide in COPY_IDES:
            process(ide)
