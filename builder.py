import os
from config import COPY_RESULT_TEMP, READY_IMG
import struct

from progress import print_progress


def build_img_archive(folder_path, archive_path):
    with open(archive_path, 'wb') as archive_file:
        # Пишем сигнатуру архива (VER2)
        archive_file.write(b'VER2')

        # Получаем список файлов в папке
        files = os.listdir(folder_path)
        num_files = len(files)

        # Пишем общее количество элементов
        archive_file.write(struct.pack('<I', num_files))

        k = 1
        total = len(files)
        for filename in files:
            file_path = os.path.join(folder_path, filename)
            # Определяем размер файла в блоках
            file_size_blocks = (os.path.getsize(file_path) + 2047) // 2048
            # Записываем смещение файла (в блоках)
            archive_file.write(struct.pack('<I', archive_file.tell() // 2048))
            # Записываем размер файла (в блоках)
            archive_file.write(struct.pack('<I', file_size_blocks))
            # Записываем имя файла
            archive_file.write(filename.encode('ascii').ljust(24, b'\0'))
            # Записываем содержимое файла
            with open(file_path, 'rb') as file:
                archive_file.write(file.read())
            # Заполняем нулями оставшиеся блоки файла, если необходимо
            padding_size = file_size_blocks * 2048 - os.path.getsize(file_path)
            if padding_size > 0:
                archive_file.write(b'\0' * padding_size)

            print_progress(
                k,
                total,
                f"File: {filename}"
            )
            k += 1



if __name__ == '__main__':
    build_img_archive(COPY_RESULT_TEMP, READY_IMG)
