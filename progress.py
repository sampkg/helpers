from time import sleep


def print_progress(count, total, status='', prefix=""):
    percent = (count / float(total))
    bar_length = int(20 * percent)
    bar = '#' * bar_length + '-' * (20 - bar_length)
    display = int(percent * 100)
    if display < 10:
        display = ' ' + str(display)

    print(f"\r{prefix}{display}% [{bar}] - {status} ({count}/{total})", end="")
    if count == total:
        print(f"\r{prefix}100% [{bar}] - Ready", end="")


if __name__ == "__main__":
    for i in range(100):
        print_progress(i, 100, "test")
        sleep(0.1)
